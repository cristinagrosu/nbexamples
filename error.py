"""
Errors and exceptions.
"""


class PathOutsideRoot(Exception):
    pass


class CorruptedFile(Exception):
    pass
